package com.company.quack;

/**
 * Created by Arthur on 16.06.2017.
 */
public interface QuackBehavior {
    public void quack();
}
