package com.company.quack.quackBehaviors;

import com.company.quack.QuackBehavior;

/**
 * Created by Arthur on 20.06.2017.
 */
public class MuteQuack implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("<Silence>");
    }
}
