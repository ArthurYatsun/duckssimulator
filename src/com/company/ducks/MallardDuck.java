package com.company.ducks;

import com.company.Duck;
import com.company.fly.flyBehaviors.FlyWithWings;
import com.company.quack.quackBehaviors.Quack;

/**
 * Created by Arthur on 20.06.2017.
 */
public class MallardDuck extends Duck {

    public MallardDuck(){
        quackBehavior = new Quack();
        flyBehavior = new FlyWithWings();
    }

    @Override
    public void dispaly() {
        System.out.println("Display for MallardDuck");
    }
}
