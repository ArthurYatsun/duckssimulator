package com.company.ducks;

import com.company.Duck;

/**
 * Created by Arthur on 20.06.2017.
 */
public class ReadHeadDuck extends Duck {
    @Override
    public void dispaly() {
        System.out.println("Display for ReadHeadDuck");
    }
}
