package com.company;

import com.company.ducks.MallardDuck;

/**
 * Created by Arthur on 20.06.2017.
 */
public class Main {

    public static void main(String[] args){
        Duck mallard = new MallardDuck();
        mallard.performQuack();
        mallard.performFly();
    }
}
