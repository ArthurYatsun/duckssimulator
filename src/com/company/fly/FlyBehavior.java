package com.company.fly;

/**
 * Created by Arthur on 16.06.2017.
 */
public interface FlyBehavior {
    public void fly();
}
