package com.company.fly.flyBehaviors;

import com.company.fly.FlyBehavior;

/**
 * Created by Arthur on 20.06.2017.
 */
public class FlyWithWings implements FlyBehavior{
    @Override
    public void fly() {
        System.out.println("I'm flying");
    }
}
