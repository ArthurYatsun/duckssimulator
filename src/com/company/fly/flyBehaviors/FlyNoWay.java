package com.company.fly.flyBehaviors;

import com.company.fly.FlyBehavior;

/**
 * Created by Arthur on 16.06.2017.
 */
public class FlyNoWay implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("Can't fly");
    }
}
