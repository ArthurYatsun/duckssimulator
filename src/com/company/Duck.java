package com.company;

import com.company.fly.FlyBehavior;
import com.company.quack.QuackBehavior;

public abstract class Duck {

    //переменные с типом интерфейса для
    public QuackBehavior quackBehavior;
    public FlyBehavior flyBehavior;


    public void performQuack(){
        quackBehavior.quack();
    }


    public void performFly(){
        flyBehavior.fly();
    }

    //динамическое изменение поведения для полета уток
    public void setFlyBehavior(FlyBehavior fb){
        flyBehavior = fb;
    }

    //динамическое изменение поведения для кваканья уток
    public void setQuackBehavior(QuackBehavior qb){
        quackBehavior = qb;
    }

    public abstract void dispaly();

    public void swim() {
        System.out.println("All ducks float");
    }
}